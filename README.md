# Framework Padawans

## Carlos Magalhães Silva

Para instalar os módulos necessários para a execução do programa:

```
$ yarn install
```

Para executar o programa:

```
$ yarn start
```
